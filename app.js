var express = require('express'),
    fs = require('fs'),
    app = express(),
    path = require('path');


var list_dir = require('./routes/list_dir');


app.use(express.static(path.join(__dirname, 'views')));
app.get('/', function (req, res) {
    var filePath = "/views/index.html";

    fs.readFile(__dirname + filePath , function (err,data){
        res.contentType("text/html");
        res.send(data);
    });
});
app.use('/list_dir', list_dir);

app.listen(8080, function(){
    console.log('Listening on 8080');
});