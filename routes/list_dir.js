var express = require('express');
var router = express.Router();
var path      = require("path");
var fs        = require("fs");
var mime = require('mime');

var rootPath = __dirname+"/../files";

router.get('/', function(req, res, next) {
    console.log(req.query);
    var queryPath = req.query.path?req.query.path:"/" //if there is a valid query path else use '/'
    listDir(queryPath, function(err, list) {
        if(err)
        {
            console.error(err)
        }
        else {
            console.log('list:', list);
            res.end(JSON.stringify({success:1,statusCode:200,response:list}))
        }
    });
});

router.get('/get_file', function (req, response) {
    var filePath = rootPath+req.query.path;
    console.log(filePath);
    var fileName = filePath.slice(filePath.lastIndexOf('/')+1,filePath.length);
    console.log(fileName);
    fs.exists(filePath, function(exists){
        if (exists) {
            var type = mime.lookup(filePath)  //to check type of files
            console.log(type);

            if(type && !isDownlaodableFile(type)) {
                response.writeHead(200, {
                    "Content-Type": type
                });
                fs.createReadStream(filePath).pipe(response);
            }
            else {
                response.writeHead(200, {
                    "Content-Type": "application/octet-stream",
                    "Content-Disposition": "attachment; filename=" + fileName
                });
                fs.createReadStream(filePath).pipe(response);
            }
        } else {
            response.writeHead(400, {"Content-Type": "text/plain"});
            response.end("ERROR File does NOT Exists");
        }
    });

});

function isDownlaodableFile(type) {
    //add files format which are suppose to be downlaoded
    var downloadableFiles = ["application/octet-stream","application/x-sql"]
    return downloadableFiles.indexOf(type)>-1? true:false;
}

//get file and folders description and add them to array
function listDir(filePath, getfiles) {
    var arr = [];

    filePath = rootPath+filePath;
    console.log("filePath",filePath);

    fs.readdir(filePath,function(err, files){
        if (err) {
            return console.error(err);
        }
        files.forEach( function (file){
            console.log( file );
            var stats = fs.statSync(filePath+"/"+file);

            var type = stats.isFile()?"file":(stats.isDirectory()?"folder":"unknown");
            console.log(stats);
            if(type=="file" || type == "folder") {
                var size = (type == "file") ? stats.size : 0;
                arr.push({name: file, type: type, size:size});
            }

        });
        getfiles(null,arr);
    });
}



module.exports = router;
