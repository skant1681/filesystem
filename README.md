# README #


### About Repo ###

* A file system application

### How do I get set up? ###

* clone the repo in your local machine
* go to repo and run command <npm install>
* run command to start server <node app.js>
* Open url in browser http://localhost:8080/

### Project   Structure ###

* app.js
* views --> index.html
* 		--> app.js
* files
* routes --> list_dir.js
