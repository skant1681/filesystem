

const config = {
    baseUrl: "http://localhost:8080"
};
var globalPath ="";

function fetchFileDetails(path) {
    globalPath = path
    $('tbody').html("");
    var settings = {
        "url": config.baseUrl+"/list_dir?path="+path,
        "method": "GET",
        "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        }
    };
    appendCurrentPath();
    $.ajax(settings).done(function (data) {
        console.log(data);
        var response;
        try {
            response = JSON.parse(data).response;
        }
        catch (ex){
            response = data.response;
        }
        for(var key in response) {
            appendRow(key,response[key]);
        }
    });
}

function fetchFile(path) {
    window.open(config.baseUrl+"/list_dir/get_file?path="+path);
}

function openFileOrFolder(path,type) {
    if(type=="folder") {
        fetchFileDetails(path)
    }
    else {
        fetchFile(path)
    }
}

function appendRow(id,file) {

    var rowHTML ='<tr onclick="openFileOrFolder(\''+globalPath+'/'+file.name+'\',\''+file.type+'\')" style="cursor: pointer">\n' +
        '        <th scope="row">'+id+'</th>\n' +
        '        <td>'+file.type+'</td>\n' +
        '        <td><a href="#">'+file.name+'</a></td>\n' +
        '        <td>'+file.size+'</td>\n' +
        '        </tr>'
        $('tbody').append(rowHTML);
}

fetchFileDetails("");

function appendCurrentPath() {
    var currentPath = "Current Path: "+ (globalPath?globalPath:"/");
    $("#curret-dir").html(currentPath);
}
appendCurrentPath();