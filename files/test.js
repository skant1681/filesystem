const config = {
    baseUrl: "http://localhost:8080"
};
var path ="";

function fetchFileDetails(path) {
    $('tbody').html("");
    var settings = {
        "url": config.baseUrl+"/list_dir?path="+path,
        "method": "GET",
        "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        }
    };

    $.ajax(settings).done(function (data) {
        console.log(data);
        var response;
        try {
            response = JSON.parse(data).response;
        }
        catch (ex){
            response = data.response;
        }
        for(var key in response) {
            appendRow(key,response[key]);
        }
    });
}

function fetchFile(path) {
    // http://localhost:8080/list_dir/get_file?path=/2.png
    window.open(config.baseUrl+"/list_dir/get_file?path="+path);
    // var settings = {
    //     "url": config.baseUrl+"/list_dir/get_file?path="+path,
    //     "method": "GET",
    //     "headers": {
    //         "content-type": "application/x-www-form-urlencoded",
    //         "cache-control": "no-cache"
    //     }
    // };
    //
    // $.ajax(settings).done(function (data) {
    //     window.URL.revokeObjectURL(data);
    //     console.log(data);
    // });
}

function openFileOrFolder(path,type) {
    if(type=="folder") {
        fetchFileDetails(path)
    }
    else {
        fetchFile(path)
    }
}

function appendRow(id,file) {

    var rowHTML ='<tr onclick="openFileOrFolder(\''+path+'/'+file.name+'\',\''+file.type+'\')" style="cursor: pointer">\n' +
        '        <th scope="row">'+id+'</th>\n' +
        '        <td>'+file.type+'</td>\n' +
        '        <td><a href="#">'+file.name+'</a></td>\n' +
        '        <td>'+file.size+'</td>\n' +
        '        </tr>'
    $('tbody').append(rowHTML);
}

fetchFileDetails("");